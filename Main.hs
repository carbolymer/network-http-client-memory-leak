module Main where

import           Control.Concurrent       (ThreadId, threadDelay)
import           Control.Concurrent.Async
import           Control.Concurrent.QSem
import           Control.Exception
import           Control.Monad
import           Data.Foldable            (for_)
import           Data.IORef               (IORef, atomicModifyIORef', newIORef)
import           GHC.Conc                 (forkIO, killThread)
import           System.Environment
import           System.Mem.Weak          (deRefWeak, mkWeakPtr)
import qualified System.Remote.Monitoring as Monitoring
import           UnliftIO.Async           (pooledForConcurrentlyN_)
import           UnliftIO.MVar            (MVar, newEmptyMVar, readMVar,
                                           tryPutMVar)

main :: IO ()
main = do
  _ <- Monitoring.forkServer "0.0.0.0" 8000
  let maxConcurrency = 500
      inputList = [1..50_000] :: [Int]

  getArgs >>= \case
    ["unliftio"] -> do
      pooledForConcurrentlyN_ maxConcurrency inputList action

    ["sem"] -> do
      mapConcurrently1 maxConcurrency inputList action

    other -> error $ "unknown arguments: " <> show other

action :: a -> IO ()
action _ = threadDelay 500_000

mapConcurrently1 :: Int -> [a] -> (a -> IO ()) -> IO ()
mapConcurrently1 nConcurrent input act = do
  qsem <- newQSem nConcurrent
  asyncs <- forM input $ \x -> do
    waitQSem qsem
    a_ <- async $ do
      act x `finally` signalQSem qsem
    mkWeakPtr a_ Nothing
  forM_ asyncs $ \wPtr -> do
    async'm <- deRefWeak wPtr
    for_ async'm wait

-- -- custom semaphore
-- mapConcurrently2 :: Int -> [a] -> (a -> IO ()) -> IO ()
-- mapConcurrently2 nConcurrent input act = do
--   qsem <- mkSemaphore nConcurrent
--   asyncs <- forM input $ \x -> do
--     waitSemaphore qsem
--     async $ do
--       act x `finally` signalSemaphore qsem
--   mapM_ wait asyncs

-- -- custom semaphore and custom async wrapper over forkIO
-- mapConcurrently3 :: Int -> [a] -> (a -> IO ()) -> IO ()
-- mapConcurrently3 nConcurrent input act = do
--   qsem <- mkSemaphore nConcurrent
--   asyncs <- forM input $ \x -> do
--     waitSemaphore qsem
--     async' $ do
--       act x `finally` signalSemaphore qsem
--   mapM_ wait' asyncs




-- data Semaphore = Semaphore
--   { concurrency :: IORef Int
--   , wakeFlag    :: MVar ()
--   }

-- mkSemaphore :: Int -> IO Semaphore
-- mkSemaphore maxConcurrency' = Semaphore <$> newIORef maxConcurrency' <*> newEmptyMVar

-- waitSemaphore :: Semaphore -> IO ()
-- waitSemaphore semaphore@(Semaphore currentConcTV wakeFlag') = do
--   canProceed <- atomicModifyIORef' currentConcTV $ \availableSlots ->
--     if availableSlots <= 0
--        then (0, False)
--        else (availableSlots - 1, True)
--   -- if cannot proceed, wait
--   unless canProceed $ do
--     readMVar wakeFlag'
--     waitSemaphore semaphore

-- signalSemaphore :: Semaphore -> IO ()
-- signalSemaphore (Semaphore currentConcTV wakeFlag') = do
--   atomicModifyIORef' currentConcTV $ \availableSlots -> (availableSlots + 1, ())
--   void $ tryPutMVar wakeFlag' ()


-- data Async' = Async'
--   { finishFlag :: MVar ()
--   , tid        :: ThreadId
--   }

-- async' :: IO a -> IO Async'
-- async' act = do
--   mv <- newEmptyMVar
--   tid' <- forkIO $ do
--     _ <- act
--     void $ tryPutMVar mv ()
--   pure $ Async' mv tid'

-- wait' :: Async' -> IO ()
-- wait' (Async' mv' tid') = do
--   readMVar mv'
--   killThread tid'
