SHELL:=/bin/bash

clean:
	stack clean

build:
	stack build --profile

run:
	stack exec --profile -- memetest-exe +RTS -A64m -H500m -I0 -T -L500 -p -s -l -h -qn8 -RTS sem

run-unliftio:
	stack exec --profile -- memetest-exe +RTS -A64m -H500m -I0 -T -L500 -p -s -l -h -qn2 -RTS unliftio

heap-plot: 
	which hp2ps
	which ps2pdf
	hp2ps -y -c -M memetest-exe.hp
	ps2pdf memetest-exe.ps
	ghc-prof-flamegraph --alloc memetest-exe.prof -o memetest-exe.svg

.PHONY: run heap-plot build clean
