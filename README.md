# A missing memory mistery

## Synopsis
When starting large amount of `forkIO` limited by a semaphore, there is a visible significant memory increase, which
is not explained by GHC profiler output.

Profiler output:
```
<<ghc: 68328518936 bytes, 132 GCs, 406694965/710120528 avg/max bytes residency (113 samples), 2378M
in use, 0.017 INIT (0.031 elapsed), 222.970 MUT (33.222 elapsed), 33.175 GC (4.233 elapsed) :ghc>>
```

##

## Prerequisites
`stack`, `python-invoke`, `hp2ps`, `ps2pdf`,
[`ghc-prof-flamegraph`](https://www.fpcomplete.com/blog/2015/04/ghc-prof-flamegraph/) installed

## Steps to reproduce
1. Build the project:
  ```
  make build
  ```

2. Run the `app/Main.hs`:
  ```
  make run
  ```
  You can go to http://localhost:8000 to observe EKG output in real time.

3. After the run you can generate the heap profile using:
  ```
  make heap-plot
  ```

4. Inspect the output files:
 - [memetest-exe.prof]()
 - [memetest-exe.pdf]()
 - [memetest-exe.svg]()
 - [memetest-hp.pdf]()



## References

1. https://downloads.haskell.org/ghc/latest/docs/html/users_guide/runtime_control.html
1. https://www.fpcomplete.com/blog/2015/04/ghc-prof-flamegraph/
1. https://www.brendangregg.com/flamegraphs.html
1. https://github.com/brendangregg/FlameGraph


<!-- vim: set textwidth=100: -->
